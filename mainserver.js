//create by kyun

var express = require('express')
var cluster = require('express-cluster');
var http = require('http');
var socketio = require('socket.io');
var bodyparser = require('body-parser');
var schedule = require('node-schedule');
var dbmodule = require('./dbmodule');
var timelog = require('./timelog');

var classcheckable = new Array(); //해당일에 출석한 강의 정보
var baduser = new Array();//앱 새로깔은 유저 차단

var dayrule = new schedule.RecurrenceRule();
dayrule.dayOfWeek = [new schedule.Range(1,6)];
dayrule.hour = 0;
dayrule.minute = 0;

var dayj = schedule.scheduleJob(dayrule, function() { //자정에 스케쥴 처리
console.log('midnight');
  var length = classcheckable.length;
  var date = timelog.getnowdate();
  var classid;
  baduser = new Array(); //db 처리도 추가 필
  for(var i = 0;i < length; i++){
    classid = classcheckable[i].classid;
    dbmodule.midnight(classid,date);
    if(i == length -1) {
      classcheckable = new Array();
      log('midnight setting');
    }
  }
});

cluster(function (worker) {
  var id = worker.id;
  dbmodule.idset(id);
  log('start');
  
  var app = express(); //서버 설정
  app.set(function(){app.use(express.bodyParser());});
  app.use(bodyparser.urlencoded({extended:false}));
  var server = http.createServer(app).listen(8107);
  var io = socketio.listen(server);
  
  var hourrule1 = new schedule.RecurrenceRule();
  hourrule1.dayOfWeek = [new schedule.Range(1,6)];
  hourrule1.minute = 1;
  
  var hourrule2 = new schedule.RecurrenceRule();
  hourrule2.dayOfWeek = [new schedule.Range(1,6)];
  hourrule2.minute = 1;
  hourrule2.second = 5;
  
  var classuuid; //서버별로 uuid를(모두 동일함)  가지고 있음
  
  dbmodule.uuidsetting(function(msg){
    classuuid = msg; //서버 시작시 uuid 저장
  });
  
  
  var hourj = schedule.scheduleJob(hourrule1, function(){//일정시간마다 uuid리셋
    dbmodule.uuidreset(function(msg){
      classuuid = msg;
    });
  });
  
  app.get('/uuidsearch/:uuid',function(req,res,next){//학생이 검색한 uuid의 확인을 요청
    var uuid = req.params.uuid;
    var result = arraycheckuuid(uuid,classuuid);
    res.set('Content-Type', 'text/plain');
    if(result == -1){
      res.send('notclass');
      res.end();
      log('class donot exsist');
    }
    else {
      res.send(classuuid[result].classname);
      res.end();
      log('search success uuid class ' + classuuid[result].classname,id);
    }
  });
  
  app.get('/getlist/:userid',function(req,res,next) { //학생 출석기록
    var userid = req.params.userid;
    
    dbmodule.getuserattendance(userid,function(msg) {
      if(msg == 'null'){
        log('user ' + userid + ' is no one attendance list');
        res.set('Content-Type', 'text/plain');
        res.send(null);
        res.end();
      }
      else {
        res.set('Content-Type', 'application/json; charset=utf-8');
        res.send(msg);
        log('to user ' + userid + ' send attendance list');
        res.end();
      }
    });
  });
  
  app.get('/getuserclass/:userid',function(req,res,next) {
    var userid = req.params.userid;
    
    dbmodule.getuserclasslist(userid,function(msg) {
      if(msg == '[]'){
        log('user ' + userid + ' is no one class list');
        res.set('Content-Type', 'application/json; charset=utf-8');
        res.send(msg);
        res.end();
      }
      else {
        res.set('Content-Type', 'application/json; charset=utf-8');
        res.send(msg);
        log('to user ' + userid + ' send class list');
        res.end();
      }
    });
  });
  
  app.post('/attendance', function(req,res,next){ // 출석체크
    var useruuid = req.body.uuid;
    var userinfo = req.body.studentid;
    var result;
    log('student ' + userinfo + ' connection to ' + useruuid,id);
    res.set('Content-Type', 'text/plain');
    checkbaduser(userinfo,function(msg) {
      if(msg == 'bad'){
        res.send('bad');
        log('bad user');
        res.end
      }
      else {
        dbmodule.attendance(useruuid,userinfo,function(msg){
          result = msg;
          log(result);
          res.send(result);
          res.end();
        });
      }
    });
  });
  
  app.post('/setclientid',function(req,res,next){ //어플리케이션 등록
    var appuuid = req.body.AUID;
    var userinfo = req.body.studentid;
    var result;
    log('student ' + userinfo + ' request application registration');
    res.set('Content-Type', 'text/plain');
    dbmodule.setclientid(appuuid,userinfo,function(msg){
      result = msg;
      log(result);
      res.send(result);
      if(result == 'dfail'){
        baduser[baduser.length] = userinfo;
      }
      res.end();
    });
  });
  
  app.post('/checkclientid',function(req,res,next){
    var appuuid = req.body.AUID;
    var userinfo = req.body.studentid;
    var result;
    log('student ' + userinfo + ' check application registration');
    res.set('Content-Type', 'text/plain');
    dbmodule.checkclientid(appuuid,userinfo,function(msg){
      result = msg;
      log(result);
      res.send(result);
      if(result == 'dfail'){
        baduser[baduser.length] = userinfo;
      }
      res.end();
    });
  });
  
  io.on('connection',function(socket){ //비콘과 소켓 통신
    var roomid = null;
    var fc = false;
    var dointerval = false;
    var classid = null;
    var nextstart = null;
    var nstarttime = null;
    var nowtime = null;
    var lastend = null;
    var lasttime = null;
    var sday = null;
    var bmsg;
    var nmsg;
    var interval;
    function socketback(){
      var now = new Date();
      day = now.getDay();
      if(sday != day){ //하루 지나면 리셋
        sday = day;
        fc = false;
      }
      nowtime = timelog.getnowtimesec();
      if(roomid != null && fc == false){
        dbmodule.returnclass(roomid,function(msg) {
          classid = msg.classid;
          nextstart = msg.nextstart;
          lastend = msg.lasttime;
          nstarttime = null;
          lasttime = null;
          if(msg == null){ // 오늘 강의가 없는 강의실
            fc = true;
            nmsg = 'nottoday';
            if(bmsg != nmsg){
              log('classroom ' + roomid + " don't have class today");
              writeData(socket,nmsg);
              bmsg = nmsg;
            }
          }
          else {
            if(nextstart != null ) {
              nstarttime = timelog.classstarttime(nextstart);
            }
            if(lastend != null) {
              lasttime = timelog.classendtime(lastend);
            }
            if(classid == null && nextstart == null){
              nmsg = 'endtoday';
              fc = true;
              if(bmsg != nmsg){
                log('classroom ' + roomid + ' end all today schedule');
                writeData(socket,nmsg);
                bmsg = nmsg;
              }
            }
            else {
              var classset = arraycheckuuid(classid,classuuid);
              if(classset == -1){//정보에 없는 강의
                fc = true;
                nmsg = 'incorrect';
                if(bmsg != nmsg){
                  log(classid + ' is incorrect class');
                  writeData(socket,nmsg);
                  bmsg = nmsg;
                }
              }
              else {
                fc = true;
                nmsg = {classid : classid, uuid : classuuid[classset].classuuid};
                if(bmsg != nmsg){
                  log('classroom ' + roomid + ' connect and setting class ' + classid);
                  writeData(socket,nmsg);
                  bmsg = nmsg;
                }
              }
            }
          }
        });
      }
      else if ( nstarttime <= nowtime && fc == true && nstarttime != null) {
        dbmodule.returnclass(roomid,function(msg) {
          classid = msg.classid;
          nextstart = msg.nextstart;
          lastend = msg.lasttime;
          nstarttime = null;
          lasttime = null;
          if(msg == null){ // 오늘 강의가 없는 강의실
            nmsg = 'nottoday';
            if(bmsg != nmsg){
              log('classroom ' + roomid + " don't have class today");
              writeData(socket,nmsg);
              bmsg = nmsg;
            }
          }
          else {
            if(nextstart != null) {
              nstarttime = timelog.classstarttime(nextstart);
            }
            if(lastend != null) {
              lasttime = timelog.classendtime(lastend);
            }
            var classset = arraycheckuuid(classid,classuuid);
            if(classset == -1){//정보에 없는 강의
              nmsg = 'incorrect';
              if(bmsg != nmsg){
                log(classid + ' is incorrect class');
                writeData(socket,nmsg);
                bmsg = nmsg;
              }
            }
            else {
              nmsg = {classid : classid, uuid : classuuid[classset].classuuid};
              if(bmsg != nmsg){
                log('classroom ' + roomid + ' connect and setting class ' + classid);
                writeData(socket,nmsg);
                bmsg = nmsg;
              }
            }
          }
        });
      }
      else if(fc == true && lasttime != null && lasttime <= nowtime) {
        nmsg = 'endtoday';
        if(bmsg != nmsg){
          log('classroom ' + roomid + ' end all today schedule');
          lasttime = null;
          classid = null;
          writeData(socket,nmsg);
          bmsg = nmsg;
        }
      }
    }
    
    var uuidresetting = schedule.scheduleJob(hourrule2, function(){
      if(roomid != null && classid != null){
        var classset = arraycheckuuid(classid,classuuid);
        if(classset == -1){//정보에 없는 강의
          nmsg = 'incorrect';
          if(bmsg != nmsg){
            log(classid + ' is incorrect class');
            writeData(socket,nmsg);
            bmsg = nmsg;
          }
        }
        else {
          nmsg = {classid : classid, uuid : classuuid[classset].classuuid};
          if(bmsg != nmsg){
            log('classroom ' + roomid + ' reset uuid class ' + classid);
            writeData(socket,nmsg);
            bmsg = nmsg;
          }
        }
      }
    });
      
    socket.on('message',function(message){
      roomid = message;
      setTimeout(function(){ 
        if(dointerval == false){
          socketback();
          interval = setInterval(function(){socketback()},1000);
          dointerval == true;
        }
      },100);
    });
    
    socket.on('disconnect', function() {
      clearTimeout(interval);
      if(roomid != null) {
        log('socketid ' + socket.id + ' classroom ' + roomid + ' disconnecting');
      }
      else {
        log('socketid ' + socket.id + ' disconnect');
      }
    });
  });
  function log(data) {
    timelog.log(data,id);
  }
}, {count:1});

function writeData(socket, data) {//소켓 통신 send
    var success = !socket.write(data);
    if (!success) {
        (function (socket, data) {
            socket.once('drain', function () {
                writeData(socket, data);
            })
        })(socket, data);
    }
}

function arraycheckuuid(data,classuuid) {
  for(var i = 0;i < classuuid.length ;i ++){
    if(classuuid[i].classuuid == data || classuuid[i].classid == data){
      return i;
    }
    else if (i == classuuid.length - 1){
      return -1;
    }
  }
}

function checkbaduser(userid,callback) {
  if(baduser.length == 0){
    if(typeof callback === 'function') {
        callback('good');
    }
  }
  else {
    for(var i = 0; i<baduser.length; i++){
      if(baduser[i] == userid){
        if(typeof callback === 'function') {
          callback('bad');
        }
      }
      else if(i == baduser.length -1){
        if(typeof callback === 'function') {
          callback('good');
        }
      }
    }
  }
}