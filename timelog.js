

exports.log = function (data,id){
  var now = getnowtime();
  console.log(now + ' : ' + 'no' + id + ' server ' + data);
}

exports.mlog = function(data){
  var now = getnowtime();
  console.log(now + ' : ' + data);
}

exports.getnowdate = getnowdate;
exports.getnowtime = getnowtime;
exports.getnowtimewithdata = getnowtimewithdata;
exports.checkclassday = checkclassday;
exports.checkclasswithoutday = checkclasswithoutday;
exports.classstarttime = classstarttime;
exports.classendtime = classendtime;
exports.getnowtimesec = getnowtimesec;

function getnowdate(){
  now = new Date();
  year = now.getFullYear();
  month= now.getMonth() + 1;
  if(month < 10){
    month = '0' + month;
  }
  date = now.getDate();
  if(date<10){
    date = '0' + date;
  }
  
  var time = year + '-' + month + '-' + date;
  return time;
}

function getnowtime(){
  now = new Date();
  year = now.getFullYear();
  month= now.getMonth() + 1;
  if(month < 10){
    month = '0' + month;
  }
  date = now.getDate();
  if(date<10){
    date = '0' + date;
  }
  hour = now.getHours();
  if(hour<10){
    hour = '0' + hour;
  }
  min = now.getMinutes();
  if(min<10){
    min = '0' + min;
  }
  sec = now.getSeconds();
  if(sec<10){
    sec = '0' + sec;
  }
  
  var time = year + '-' + month + '-' + date + ' ' + hour + ':' + min + ':' + sec;
  return time;
}

function getnowtimewithdata(now){
  year = now.getFullYear();
  month= now.getMonth() + 1;
  if(month < 10){
    month = '0' + month;
  }
  date = now.getDate();
  if(date<10){
    date = '0' + date;
  }
  hour = now.getHours();
  if(hour<10){
    hour = '0' + hour;
  }
  min = now.getMinutes();
  if(min<10){
    min = '0' + min;
  }
  sec = now.getSeconds();
  if(sec<10){
    sec = '0' + sec;
  }
  
  var time = year + '-' + month + '-' + date + ' ' + hour + ':' + min + ':' + sec;
  return time;
}

function checkclassday(day,start,end){
  now = new Date();
  mday = now.getDay();
  hour = now.getHours();
  min = now.getMinutes();
  var time = hour * 3600 + min * 60;
  var starttime = classstarttime(start);
  var latetime = starttime + 1200;
  var endtime = classendtime(end);
  if(mday == day){
    if(time < starttime){ //수업시작 전
      return 1;
    }
    else if(time >= starttime && time < latetime ){ //수업시작후 20분동안 출석체크 인정
      return 0;
    }
    else if(time >= latetime && time <= endtime) { //수업시작후 20분 후 지각 체크
      return 2;
    }
    else if(time > endtime){ //결석
      return 3;
    }
  }
  else{ //해당 날짜가 아님
    return -1;
  }
}

function checkclasswithoutday(start,end){
  now = new Date();
  hour = now.getHours();
  min = now.getMinutes();
  sec = now.getSecond();
  var time = hour * 3600 + min * 60 + sec;
  var starttime = classstarttime(start);
  var latetime = starttime + 1200; // 지각시간 설정
  var endtime = classendtime(end);
  if(time < starttime){ //수업시작 전
    return 1;
  }
  else if(time >= starttime && time < latetime ){ //수업시작후 20분동안 출석체크 인정
    return 0;
  }
  else if(time >= latetime && time <= endtime) { //수업시작후 20분 후 지각 체크
    return 2;
  }
  else if(time > endtime){ //결석
    return 3;
  }
}


function classstarttime(time) {
  if(time == 0) return 28800;
  else if(time == 1 ) return 32400;
  else if(time == 2 ) return 36000;
  else if(time == 3 ) return 39600;
  else if(time == 4 ) return 43200;
  else if(time == 5 ) return 46800;
  else if(time == 6 ) return 50400;
  else if(time == 7 ) return 54000;
  else if(time == 8 ) return 57600;
  else if(time == 9 ) return 61200;
  else if(time == 10 ) return 64800;
  else if(time == 11 ) return 68100;
  else if(time == 12 ) return 71400;
  else if(time == 13 ) return 74700;
  else if(time == 14 ) return 78000;
  else if(time == 99 ) return 0;
}


function classendtime(time){
  if(time == 0) return 31800;
  else if(time == 1 ) return 35400;
  else if(time == 2 ) return 39000;
  else if(time == 3 ) return 42600;
  else if(time == 4 ) return 46200;
  else if(time == 5 ) return 49800;
  else if(time == 6 ) return 53400;
  else if(time == 7 ) return 57000;
  else if(time == 8 ) return 60600;
  else if(time == 9 ) return 64200;
  else if(time == 10 ) return 67800;
  else if(time == 11 ) return 71100;
  else if(time == 12 ) return 74400;
  else if(time == 13 ) return 77700;
  else if(time == 14 ) return 81000;
  else if(time == 99 ) return 86360;
}


function getnowtimesec(){
  var now = new Date();
  hour = now.getHours();
  min = now.getMinutes();
  var time = hour * 3600 + min * 60
  return time;
}