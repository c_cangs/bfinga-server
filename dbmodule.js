
var mysql = require('mysql');
var uuid = require('node-uuid');
var timelog = require('./timelog');
var sid;

var client = mysql.createPool({ //db연동
  host :'localhost',
  port : 3306,
  user:'root',
  password:'1234',
  database : 'fingerserver'
});

function log(data) {
  timelog.log(data,sid);
}

function mlog(data) {
  timelog.mlog(data);
}

exports.idset = function (id){
  sid = id;
}

exports.uuidsetting = function (callback) {
  var classuuid = new Array();
  client.getConnection(function(err,connection){  //서버 가동시 작동
    connection.query('select classuuid from class where classuuid is null', function(err, rows) {
      if (err){
        log(err.messge);
      } 
      else {
        if(rows.length == 0 ){
          connection.query('SELECT classid,classuuid,classname from class', function(err, rows) {
            if (err){ 
              log(err.messge);
            }
            else {
              var count = rows.length;
              for(var i = 0 ; i < count ; i++){
                var newuuid = rows[i].classuuid;
                var newclass = rows[i].classid;
                var newname = rows[i].classname;
                classuuid[i] = {classuuid : newuuid ,classid : newclass , classname : newname};
                if(i == count - 1) {
                  log('uuid saving complite');
                  if(typeof callback === 'function') {
                    callback(classuuid);
                  }
                }
              }
            }
          });
        }
        else {
          connection.query('SELECT classid , classname from class', function(err, rows) {
            if (err){ 
              log(err.messge);
            }
            else {
              var count = rows.length;
              for(var i = 0;i < count;i++){
                var byte = uuid.parse(uuid.v4());
                var newuuid = uuid.unparse(byte);
                var newclass = rows[i].classid;
                var newname = rows[i].classname;
                classuuid[i] = {classuuid : newuuid ,classid : newclass , classanem : newname};
                connection.query('UPDATE class set classuuid = ? where classid = ?',
                  [newuuid,newclass],function(err,result){
                  if(err) {
                    log(err.messge)
                  }
                });
                if(i == count - 1) {
                   log('uuid reset complite');
                   if(typeof callback === 'function') {
                    callback(classuuid);
                  }
                }
              }
            }
          });
        }
      }
    });
    connection.release();
  });
}

exports.uuidreset = function(callback) {
  var classuuid = new Array();
  client.getConnection(function(err,connection){
    connection.query('SELECT classid,classname from class', function(err, rows) {
      if (err){ 
        log(err.messge);
      }
      else {
        var count = rows.length;
        for(var i = 0;i < count;i++){
          var byte = uuid.parse(uuid.v4());
          var newuuid = uuid.unparse(byte);
          var newclass = rows[i].classid;
          var newname = rows[i].classname;
          classuuid[i] = {classuuid : newuuid ,classid : newclass, classname : newname};
          connection.query('UPDATE class set classuuid = ? where classid = ?',
            [newuuid,newclass],function(err,result){
            if(err) {
              log(err.messge)
            }
          });
          if(i == count - 1) {
            log('uuid reset');
            if(typeof callback === 'function') {
              callback(classuuid);
            }
          }
        }
      }
    });
    connection.release();
  });
}

exports.attendance = function(useruuid,userinfo,callback){
  log(useruuid+' '+userinfo);
  client.getConnection(function(err,connection){
    connection.query('select classday,classstart,classend from classlist join class where classlist.classid = class.classid and classuuid = ?;',
    [useruuid], function(err,rows) {
      if(err){
        log(err.message);
        if(typeof callback === 'function') {
          callback('fail');
        }
      }
      else {
        var check;
        if(rows.length == 0){
          log('class donot exsist');
          if(typeof callback === 'function') {
            callback('notclass');
          }
        }
        else {
          for(var i = 0;i < rows.length ; i++){
            var checkcorrectday = timelog.checkclassday(rows[i].classday,rows[i].classstart,rows[i].classend);
            if(checkcorrectday == 1){//수업시작 전
              log('study donot start yet');
              if(typeof callback === 'function') {
                callback('notyet');
              }
            }
            else if(checkcorrectday == 0){//출석
              check = 1;
              break;
            }
            else if(checkcorrectday == 2) {//지각
              check = 2;
              break;
            }
            else if(checkcorrectday == 3){ //결석
              check = 0;
              break;
            }
            else if(checkcorrectday == -1 && i == rows.length - 1){//출석일자가 아님
              log('study donot start today');
              if(typeof callback === 'function') {
                callback('nottoday');
              }
            }
          }
          if(check == 0 || check == 1 || check == 2){
            connection.query('select attendancelist.classid As classid from attendancelist join class where classuuid = ? and studentid = ? and attendancelist.classid = class.classid ;',
            [useruuid,userinfo], function(err,rows) {
              if(err){
                log(err.message);
                if(typeof callback === 'function') {
                  callback('fail');
                }
              }
              else {
                if(rows.length == 0){
                  log('not match class and user');
                  if(typeof callback === 'function') {
                    callback('not match class and user');
                  }
                }
                else {
                  var getclass = rows[0].classid;
                  log(userinfo + ' login');
                  connection.query('SELECT datetime,result from attendance where studentid = ? and classid = ?',
                  [userinfo,getclass], function(err, rows) {
                    if (err){
                      log(err.messge);
                      if(typeof callback === 'function') {
                        callback('fail');
                      }
                    }
                    else {
                      if(rows.length > 0){
                        var length = rows.length - 1;
                        var dbdayset = new Date(new Date(rows[length].date).toLocaleString());
                        var dbday = timelog.getnowtimewithdata(dbdayset).substring(0,10);
                        var today = timelog.getnowtime().substring(0,10);
                        if(dbday == today && (rows[length].classcheck == 1 || rows[length].classcheck == 2)){
                          log(userinfo + ' already check ' + getclass + ' class today');
                          if(typeof callback === 'function') {
                            callback('already');
                          }
                        }
                        else {
                          var time = timelog.getnowtime();
                          var setting = {'studentid':userinfo,'classid':getclass,'datetime':time,'result':check};
                          connection.query('INSERT INTO attendance set ?',setting,function(err,result){
                            if(err) {
                              log(err.messge)
                              if(typeof callback === 'function') {
                                callback('fail');
                              }
                            }
                            else {
                              log(userinfo + ' check complite');
                              if(check == 1){
                                if(typeof callback === 'function') {
                                  callback('ok');
                                }
                              }
                              else if(check == 2){
                                if(typeof callback === 'function') {
                                  callback('late');
                                }
                              }
                              else if(check == 0){
                                if(typeof callback === 'function') {
                                  callback('toolate');
                                }
                              }
                            }
                          });
                        }
                      }
                      else {
                        var time = timelog.getnowtime();
                        var setting = {'studentid':userinfo,'classid':getclass,'datetime':time,'result':check};
                        connection.query('INSERT INTO attendance set ?',setting,function(err,result){
                          if(err) {
                            log(err.messge)
                            if(typeof callback === 'function') {
                              callback('fail');
                            }
                          }
                          else {
                            log(userinfo + ' check complite');
                            if(check == 1){
                              if(typeof callback === 'function') {
                                callback('ok');
                              }
                            }
                            else if(check == 2){
                              if(typeof callback === 'function') {
                                callback('late');
                              }
                            }
                            else if(check == 0){
                              if(typeof callback === 'function') {
                                callback('toolate');
                              }
                            }
                          }
                        });
                      }
                    }
                  });
                } 
              }
            });
          }
        }
      }
    });
    connection.release();
  });
}

exports.setclientid = function(appuuid,userinfo,callback){
  log(appuuid);
  client.getConnection(function(err,connection) {
    connection.query('select AUID from stukeys where studentid = ?',
    [userinfo], function(err,rows) {
      if(err) {
        log(err.message);
      }
      else {
        if(rows.length == 0){
          var setting = {'studentid':userinfo,'AUID':appuuid};
          connection.query('INSERT INTO stukeys set ?',setting,function(err,result){
            if(err) {
              log(err.message);
              if(typeof callback === 'function') {
                callback('fail');
              }
            }
            else {
              log('registration success');
              if(typeof callback === 'function') {
                callback('suc');
              }
            }
          });
        }
        else {
          if(rows[0].AUID == appuuid) {
            log('reregistrate this user');
            if(typeof callback === 'function') {
                callback('alr');
            }
          }
          else {
            log('registration fail');
            if(typeof callback === 'function') {
                callback('dfail');
            }
          }
        }
      }
    });
    connection.release();
  }); 
}

exports.checkclientid = function(appuuid,userinfo,callback){
  client.getConnection(function(err,connection) {
    connection.query('select AUID from stukeys where studentid = ?',
    [userinfo], function(err,rows) {
      if(err) {
        log(err.message);
        if(typeof callback === 'function') {
          callback('fail');
        }
      }
      else {
        if(rows.length == 0){
          if(typeof callback === 'function') {
            callback('no');
          }
        }
        else {
          if(rows[0].AUID == appuuid) {
            log('confirm user '+userinfo);
            if(typeof callback === 'function') {
                callback('suc');
            }
          }
          else {
            log('registration fail');
            if(typeof callback === 'function') {
                callback('dfail');
            }
          }
        }
      }
    });
    connection.release();
  }); 
}

exports.startclassattendance = function(message){
  client.getConnection(function(err,connection){
    connection.query('select countstudydays from class where classid = ?',
    [message], function(err,rows) {
      if(err) {
        log(err.messge);
      }
      else {
        var addcount = rows[0].countstudydays + 1;
        connection.query('UPDATE class set countstudydays = ? where classid = ?',[addcount,message],function(err,result){
          if(err) {
            log(err.messge);
          }
          else {
            log('class ' + message + ' start today classcheck');
          }
        });
      }
    });
    connection.release();
  });
}


exports.midnight = function(data,date) {
  var setting, time;
  client.getConnection(function(err,connection){
    connection.query('select studentid from attendancelist where classid = data', function(err,rows) {
      if(err){
        mlog(err.message);
      }
      else {
        mlog('1');
        for(var i = 0;i < rows.length ; i++){
          connection.query("select studentid,classid,date_format(datetime,'%Y-%m-%d') As date from attendance where studentid = ? and classid = ? and date_format(datetime,'%Y-%m-%d') = ?;",
          [rows[i].studentid,data,date], function(err,rows) {
            if(err){
              mlog(err.message);
            }
            else {
              mlog('2');
              if(rows.length == 0){
                setting = {'studentid':rows[i].studentid,'classid':data,'datetime':time,'result':0};
                time = timelog.getnowtime();
                connection.query('INSERT INTO attendance set ?',setting,function(err,result) {
                  if(err) {
                    mlog(err.message);
                  }
                });
              }
            }
          });
          if(i = rows.length - 1){
            mlog('end class' + data + ' today attendancing');
          }
        }
      }
    });
  connection.release();
  });
}

exports.returnclass = function(roomid,callback) { //강의실에 수업정보 리턴
  var now = new Date();
  var today = now.getDay();
  var time = timelog.getnowtimesec();
  var starttime;
  var endtime;
  var resultid = null;
  var resultnexstart = null;
  var resultlasttime = null;
  client.getConnection(function(err,connection) {
    connection.query('select classstart,classend,classid from classlist where roomid = ? and classday = ?',
    [roomid,today],function(err,rows) {
      if(err){
        log(err.message);
      }
      else {
        if(rows.length == 0){
          if(typeof callback === 'function') {
            callback('null');
          }
        }
        var middlestart = null;
        var nowend = null;
        var nowendtime = null;
        for(var i = 0;i < rows.length;i++){
          starttime = timelog.classstarttime(rows[i].classstart);
          endtime = timelog.classendtime(rows[i].classend);
          if(time >= starttime - 600 && time < endtime + 600){
            resultid = rows[i].classid;
            nowend = rows[i].classend;
            nowendtime = timelog.classendtime(rows[i].classend);
            break;
          }
        }
        if(resultid == null) {
          for(var i = 0;i < rows.length;i++){
            if(resultid == null && time < starttime){
              middlestart = starttime;
              resultid = rows[i].classid;
              resultnexstart = rows[i].classstart;
            }
            else if (starttime < middlestart && time < starttime){
              middlestart = starttime;
              resultid = rows[i].classid;
              resultnexstart = rows[i].classstart;
            }
            if(i == rows.length -1){
              if(typeof callback === 'function') {
                callback({classid : resultid, nextstart : resultnexstart, lasttime : resultlasttime});
              }
            }
          }
        }
        else {
          for(var i = 0;i < rows.length;i++){
            if(nowendtime < starttime && middlestart == null){
              middlestart = starttime;
              resultnexstart = rows[i].classstart;
            }
            else if (starttime < middlestart && nowendtime < starttime){
              middlestart = starttime;
              resultnexstart = rows[i].classstart;
            }
            if(i == rows.length -1){
              if(resultnexstart == null){
                resultlasttime = nowend;
              }
              if(typeof callback === 'function') {
                  callback({classid : resultid, nextstart : resultnexstart, lasttime : resultlasttime});
                }
            }
          }
        }
      }
    });
    connection.release();
  });
}

exports.getuserattendance = function(userid,callback) {
  client.getConnection(function(err,connection){
    connection.query('select attendance.classid As classid, classname, datetime, result from attendance join class where studentid = ? and attendance.classid = class.classid',
    [userid],function(err,rows) {
      if(err) {
        log(err.message);
      }
      else {
        if(rows.length == 0){
          if(typeof callback === 'function') {
            callback('null');
          }
        }
        else {
          if(typeof callback === 'function') {
            callback(rows);
          }
        }
      }
    });
    connection.release();
  });
}

exports.getuserclasslist = function(userid,callback) {
  client.getConnection(function(err,connection){
    connection.query('select classid from attendancelist where studentid = ?',
    [userid],function(err,rows) {
      if(err) {
        log(err.message);
      }
      else {
        if(rows.length == 0){
          if(typeof callback === 'function') {
            callback(rows);
          }
        }
        else {
          if(typeof callback === 'function') {
            callback(rows);
          }
        }
      }
    });
    connection.release();
  });
}