var express = require('express')
  , http = require('http')
  , app = express()
  , server = http.createServer(app);
  

var uuid = require('node-uuid');

var uuidset = uuid.v4();


var io = require('socket.io').listen(server);

io.sockets.on('connection', function (socket) {
  socket.emit('example message 1', { hello: 'world 1' });

  socket.on('example message 2', function (data) {
    console.log(data);
  });
});

app.get('/', function (req, res) {
  res.send('Hello /');
});

app.get('/world.html', function (req, res) {
  res.send('Hello World');
});

app.get('/uuid', function (req, res) {
  res.send(uuidset);
});


server.listen(8107, function() {
  console.log('Express server listening on port ' + server.address().port);
});