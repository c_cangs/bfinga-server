var io = require( 'socket.io-client');
var socket = io( 'http://127.0.0.1:8107');

socket.on( 'connect',
    function() {
        console.log( 'connected!');
        socket.send('class1');
        console.log( '(sent first msg)');
    }
);

socket.on( 'message', function( data) {
    console.log( "received from server----------");
    console.log( data);
    console.log( "------------------------------");
    socket.send( 'nice to meet you too.');
});
